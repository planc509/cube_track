#include "cube_track/color_parser.h"

ColorParser::ColorParser(std::string &sub_topic, bool robot_order, bool offer_service, ros::NodeHandle &node):
    sub_topic(sub_topic), robot_order(robot_order), offer_service(offer_service), robot_face_idx(0),
    patches_cs(COLOR_SPACES), assignments(6), cube_img_cs(COLOR_SPACES),
    img_sub(node.subscribe(sub_topic, 1, &ColorParser::imgCallback, this)){
    for(int i = 0; i < patches_cs.size(); i++) {
        patches_cs[i].resize(6);
    }
    for(int i = 0; i < assignments.size(); i++) {
        assignments[i].clear();
        assignments[i].resize(9, -1);
        assignments[i][4] = i;

    }

    robot_face_order.push_back(1);
    robot_face_order.push_back(0);
    robot_face_order.push_back(2);
    robot_face_order.push_back(3);
    robot_face_order.push_back(5);
    robot_face_order.push_back(4);

    robot_rot_inc.push_back(0);
    robot_rot_inc.push_back(1);
    robot_rot_inc.push_back(2);
    robot_rot_inc.push_back(3);
    robot_rot_inc.push_back(1);
    robot_rot_inc.push_back(1);
}

ColorParser::~ColorParser() {

}

void ColorParser::extract(int face_id) {
    // Convert picture to desired color spaces
    if(HSV_IDX < cube_img_cs.size()) {
        Mat hsv;
        cvtColor(cube_img, hsv, CV_BGR2HSV);
        hsv.copyTo(cube_img_cs[HSV_IDX]);
    }

    if(LUV_IDX < cube_img_cs.size()) {
        Mat luv;
        cvtColor(cube_img, luv, CV_BGR2Luv);
        luv.copyTo(cube_img_cs[LUV_IDX]);
    }

    for(int cs_id = 0; cs_id < patches_cs.size(); cs_id++) {
        std::vector<Vec3d>* face = &patches_cs[cs_id][face_id];
        Vec3d patch;

        face->clear();
        for(int i = 0; i < sq_positions.size(); i++) {
            patch[0] = 0;
            patch[1] = 0;
            patch[2] = 0;
            int min_x = std::max(0, (int)(sq_positions[i].x-0.75*sq_lengths[i]/2));
            int max_x = std::min(cube_img_cs[cs_id].cols, (int)(sq_positions[i].x+0.75*sq_lengths[i]/2));
            int min_y = std::max(0, (int)(sq_positions[i].y-0.75*sq_lengths[i]/2));
            int max_y = std::min(cube_img_cs[cs_id].rows, (int)(sq_positions[i].y+0.75*sq_lengths[i]/2));
            for(int x = min_x; x <= max_x; x++) {
                for(int y = min_y; y < max_y; y++) {
                    Vec3b pixel = cube_img_cs[cs_id].at<Vec3b>(Point(x,y));
                    for(int j = 0; j < 3; j++) {
                        patch[j] += pixel[j];
                    }
                }
            }
            int count = (max_x-min_x+1)*(max_y-min_y+1);
            for(int j = 0; j < 3; j++) {
                patch[j] = patch[j] / count;
            }
            face->push_back(patch);
            // Will draw circles on cube_img COLOR_SPACES times
            circle(cube_img, Point(sq_positions[i].x, sq_positions[i].y), sq_lengths[i]/2, Scalar::all(255), 2);

        }
        if(robot_order) {
            rotateVector(patches_cs[cs_id][face_id], robot_rot_inc[face_id]);
        }
    }

//    std::stringstream ss;
//    ss << "Display window_extract " << cs_id << " " << face_id;
//    imshow(ss.str(), cube_img_cs[cs_id]);
//    waitKey(1);
}

double ColorParser::luv_dist(Vec3d &candidate, Vec3d &center) {
   if(candidate[0] < 100 || center[0] < 100) {
        return 300.0+std::sqrt(std::pow(candidate[1]-center[1],2)+std::pow(candidate[2]-center[2],2));
    } else {
        return std::sqrt(std::pow(candidate[1]-center[1],2)+std::pow(candidate[2]-center[2],2));
    }
}

double ColorParser::hsv_dist(Vec3d& candidate, Vec3d& center) {
    if(candidate[1] < 100 || center[1] < 100) {
        return 300.0+std::abs(candidate[0]-center[0]);
    } else {
        return std::abs(candidate[0]-center[0]);
    }
}

double ColorParser::dist(Vec3d& candidate, Vec3d& center) {
    return std::sqrt(std::pow(candidate[0]-center[0],2)+
                     std::pow(candidate[1]-center[1],2)+
                     std::pow(candidate[2]-center[2],2));
}



bool ColorParser::solve_cube(void) {
    double min_dist = DBL_MAX;
    std::vector<int> color_count(6,0);
    int assign_count = 0;
    std::vector<int> opp_face(6);
    opp_face[0] = 2;
    opp_face[1] = 3;
    opp_face[2] = 0;
    opp_face[3] = 1;
    opp_face[4] = 5;
    opp_face[5] = 4;

    std::vector<std::vector<std::vector<int> > > possibilities(6);
    for(int i = 0; i < possibilities.size(); i++) {
        possibilities[i].resize(9);
        for(int j = 0; j < possibilities[i].size(); j++) {
            possibilities[i][j].resize(6);
            for(int k = 0; k < possibilities[i][j].size(); k++) {
                possibilities[i][j][k] = k;
            }
        }
    }

    while(assign_count < 48) {
        min_dist = DBL_MAX;
        int best_face = -1;
        int best_sq = -1;
        int match_color = -1;
        int best_cs = -1;
        bool forced = false;
        for(int face = 0; face < 6; face++) {
            print_face(assignments[face]);
            for(int sq = 0; sq < 9; sq++) {
                // Unassigned, non-center square
                if(sq != 4 && assignments[face][sq] == -1 && !forced) {
                    int poss_eval = 0;
                    if(USE_RATIO_DIST) {
                        for(int cs = 0; cs < patches_cs.size(); cs++) {
                            std::vector<double> distances(6, DBL_MAX);
                            for(int k = 0; k < possibilities[face][sq].size(); k++) {
                                int cur_color = possibilities[face][sq][k];
                                if(color_count[cur_color] < 8) {
                                    if(cs == HSV_IDX) {
                                        distances[cur_color] = hsv_dist(patches_cs[cs][face][sq], patches_cs[cs][cur_color][4]);
                                    } else if(cs == LUV_IDX) {
                                        distances[cur_color] = luv_dist(patches_cs[cs][face][sq], patches_cs[cs][cur_color][4]);
                                    } else {
                                        ROS_ERROR("Shouldn't get here");
                                        distances[cur_color] = dist(patches_cs[cs][face][sq], patches_cs[cs][cur_color][4]);
                                    }
                                    poss_eval++;
                                }

                            }
                            double shortest = DBL_MAX;
                            int shortest_idx = -1;
                            for(int i = 0; i < distances.size(); i++) {
                                if(distances[i] < shortest) {
                                    shortest = distances[i];
                                    shortest_idx = i;
                                }
                            }
                            if(shortest_idx >= 0) {
                                distances.erase(distances.begin()+shortest_idx);
                                double second = DBL_MAX;
                                int second_idx = -1;
                                for(int i = 0; i < distances.size(); i++) {
                                    if(distances[i] < second) {
                                        second = distances[i];
                                        second_idx = i;
                                    }
                                }
                                if(second_idx < 0) {
                                    min_dist = shortest;
                                    best_face = face;
                                    best_sq = sq;
                                    match_color = shortest_idx;
                                    best_cs = cs;
                                } else {
                                    double ratio_dist = shortest/second;
                                    if(ratio_dist < min_dist) {
                                        min_dist = ratio_dist;
                                        best_face = face;
                                        best_sq = sq;
                                        match_color = shortest_idx;
                                        best_cs = cs;
                                    }
                                }
                            }
                        }
                        poss_eval = poss_eval / patches_cs.size();

                    } else {
                        for(int cs = 0; cs < patches_cs.size(); cs++) {
                            for(int k = 0; k < possibilities[face][sq].size(); k++) {
                                int cur_color = possibilities[face][sq][k];
                                if(color_count[cur_color] < 8) {
                                    double cur_dist = 0;
                                    if(cs == HSV_IDX) {
                                        cur_dist = hsv_dist(patches_cs[cs][face][sq], patches_cs[cs][cur_color][4]);
                                    } else if(cs == LUV_IDX) {
                                        cur_dist = luv_dist(patches_cs[cs][face][sq], patches_cs[cs][cur_color][4]);
                                    } else {
                                        ROS_ERROR("Shouldn't get here");
                                        cur_dist = dist(patches_cs[cs][face][sq], patches_cs[cs][cur_color][4]);
                                    }
                                    poss_eval++;
                                    if(cur_dist < min_dist) {
                                        min_dist = cur_dist;
                                        best_face = face;
                                        best_sq = sq;
                                        match_color = cur_color;
                                        best_cs = cs;
                                    }
                                }
                            }
                        }
                        poss_eval = poss_eval / patches_cs.size();
                    }


                    if(poss_eval == 0) {
                        ROS_ERROR("No more possibilities for face %d, square %d", face, sq);
                        return false;
                    } else if(poss_eval == 1) {
                        forced = true;
                        ROS_WARN("face %d, cube %d force to color %d", best_face, best_sq, match_color);
                    }
                }
            }
        }

        assign_count++;
        assignments[best_face][best_sq] = match_color;
        int opp_color = opp_face[match_color];
        std::vector<Point2i> neighbors;
        Point2i query(best_face, best_sq);
        get_neighbors(query, neighbors);
        for(int i = 0; i < neighbors.size(); i++) {
            std::vector<int>* poss_ptr = &possibilities[neighbors[i].x][neighbors[i].y];
            for(int j = 0; j < poss_ptr->size(); j++) {
                if((*poss_ptr)[j] == match_color) {
                    poss_ptr->erase(poss_ptr->begin()+j);
                    break;
                }
            }
            for(int j = 0; j < poss_ptr->size(); j++) {
                if((*poss_ptr)[j] == opp_color) {
                    poss_ptr->erase(poss_ptr->begin()+j);
                    break;
                }
            }
        }
        color_count[match_color]++;
        std::cout << "assign_count " << assign_count <<": face " << best_face
                  << " square " << best_sq <<" color " << match_color << " cs " << best_cs << std::endl;
        std::cout << "Possibilities: ";
        for(int i = 0; i < possibilities[best_face][best_sq].size(); i++) {
            std::cout << possibilities[best_face][best_sq][i] << " ";
        }
        std::cout << std::endl;
        std::cout << "Color count: ";
        for(int i = 0; i < color_count.size(); i++) {
            std::cout << color_count[i] << " ";
        }
        std::cout << std::endl;
        //ROS_INFO("assign_count %d: face %d square %d color %d cs %d", assign_count, best_face, best_sq, match_color, best_cs);
    }
    return true;
}

void ColorParser::print_face(std::vector<int>& face) {
    if(face.size() != 9) {
        ROS_ERROR("Face size is not nine");
        return;
    }

    for(int i = 0; i < 3; i++) {
        for(int j = 0; j < 3; j++) {
            std::cout << face[3*i+j] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

template<typename T>
void ColorParser::rotateVector(std::vector<T>& elements, int n) {
    if(elements.size() != 9) {
        ROS_ERROR("rotateVector got vector.size() != 9") ;
        return;
    }
    while(n <= 0) {
        n+= 4;
    }


    for(int i = 0; i < n; i++) {
        std::vector<T> tmp;
        tmp.push_back(elements[6]);
        tmp.push_back(elements[3]);
        tmp.push_back(elements[0]);
        tmp.push_back(elements[7]);
        tmp.push_back(elements[4]);
        tmp.push_back(elements[1]);
        tmp.push_back(elements[8]);
        tmp.push_back(elements[5]);
        tmp.push_back(elements[2]);
        tmp.swap(elements);

    }
}

int ColorParser::checkForExtract(bool wait) {

    char c = getch();
    int face_id = -1;
    if(c == EOF) {
        return 0;
    }
    if((c == 'n' || c == 'N') && robot_order) {
    	return -1;
    }
    if(c-'0' < 6 && c-'0' >= 0 && !robot_order) {
        face_id = c-'0';
    } else if(!wait || (c == ' ' && robot_order)) {
        face_id = robot_face_order[robot_face_idx];
        robot_face_idx = (robot_face_idx+1)%robot_face_order.size();
    }
    if(face_id >= 0) {
        std::cout << "Got: " << c << std::endl;

        // Extract image patches and store
        ROS_INFO("Extracting face %d", (robot_face_idx-1)%6);
        extract(face_id);

        /*std::stringstream ss;
        ss << "Display window" << (robot_face_idx-1)%6;
        imshow(ss.str(), cube_img);
        waitKey(1);*/

        return 1;

    }

    return 0;

}

int ColorParser::checkCubeReady(void) {
    // Check if we've gotten six faces yet
    bool cube_ready = true;
    int result = 0;
    for(int i = 0; i < patches_cs.size(); i++) {
        for(int j = 0; j < patches_cs[i].size(); j++) {
            if(patches_cs[i][j].size() < 9) {
                cube_ready = false;
                break;
            }
        }
    }

    if(cube_ready) {
        if(solve_cube()) {
            result = 1;
        } else {
           result = -1;
        }
        for(int i = 0; i < assignments.size(); i++) {
            if(robot_order) {
                rotateVector(assignments[i], -1*robot_rot_inc[i]);
            }

        }
        // Undo ordering
        if(robot_order) {
            std::vector<vector<int> > robot_assign;
            for(int i = 0; i < assignments.size(); i++) {
                robot_assign.push_back(assignments[robot_face_order[i]]); // Correct?
            }
            
            // Additional re-ordering
            iter_swap(robot_assign.begin()+1, robot_assign.begin()+2);
            iter_swap(robot_assign.begin()+1, robot_assign.begin()+3);
            robot_assign.swap(assignments);
        }
        
        for(int i = 0; i < assignments.size(); i++) {
            print_face(assignments[i]);

        }


    }
    return result;
}

void ColorParser::clearCubeState(void) {
    for(int i = 0; i < assignments.size(); i++) {
        assignments[i].clear();
        assignments[i].resize(9, -1);
        assignments[i][4] = i;
    }
    for(int i = 0; i < patches_cs.size(); i++) {
        for(int j = 0; j < patches_cs[i].size(); j++) {
            patches_cs[i][j].clear();
        }
    }
    //cube_img_cs.clear();
    ROS_INFO("Ready for new cube capture");
}

bool ColorParser::extractCubeService(cube_track::ExtractCube::Request &req, cube_track::ExtractCube::Response& res) {

    if(robot_order) {
        ROS_INFO("Please confirm cube face capture by pressing space, or press 'n' to return");
    } else {
        ROS_INFO("Please confirm cube face capture by pressing 0 through 5");
    }
    ros::Time start = ros::Time::now();
    ros::Time now = start;
    int extracted = 0;
    do{
        ros::spinOnce();
        extracted = checkForExtract(req.wait);
        if(extracted != 0) {
            break;
        }
        now = ros::Time::now();
    } while(ros::ok() && (req.wait_ms <= 0 || ((now.toNSec() - start.toNSec())/1000000 < req.wait_ms)));
    res.extracted = (extracted == 1);
    return true;
}

std::string ColorParser::getSolutionString(void) {
    std::string result("");
    std::vector<string> prefixes;
    prefixes.push_back("L:");
    prefixes.push_back(" R:");
    prefixes.push_back(" U:");
    prefixes.push_back(" D:");
    prefixes.push_back(" F:");
    prefixes.push_back(" B:");

    for(int i = 0; i < assignments.size(); i++) {
        std::string face_string("");
        int face_id = i;//robot_order ? robot_face_order[i] : i;
        for(int j = 0; j < assignments[face_id].size(); j++) {
            switch(assignments[face_id][j]) {
            case 0:
                face_string += "G";
                break;
            case 1:
                face_string += "W";
                break;
            case 2:
                face_string += "B";
                break;
            case 3:
                face_string += "Y";
                break;
            case 4:
                face_string += "R";
                break;
            case 5:
                face_string += "O";
                break;
            default:
                ROS_ERROR("Unrecognized assignment value %d in getSolutionString", assignments[face_id][j]);
                break;
            }
        }
        // Need to reverse last face
        if(robot_order && i == 5) {
            std::reverse(face_string.begin(), face_string.end());
        }
        result += prefixes[i] + face_string;
    }
    return result;
}

bool ColorParser::solveCubeService(cube_track::SolveCube::Request &req, cube_track::SolveCube::Response& res) {
    int solved = checkCubeReady();
    if(solved == 1) {
        // Create string and set res
        res.state = getSolutionString();
    }
    clearCubeState();
    res.solved = solved;
    return true;
}

void ColorParser::start(ros::NodeHandle& node) {
    if(offer_service) {
        ros::ServiceServer extract_server = node.advertiseService("extract_cube_face", &ColorParser::extractCubeService, this);
        ros::ServiceServer solve_server = node.advertiseService("solve_cube", &ColorParser::solveCubeService, this);
        ros::spin();
    } else {
     //   parseColor();
        if(robot_order) {
            ROS_INFO("Please confirm cube face capture by pressing space");
        } else {
            ROS_INFO("Please confirm cube face capture by pressing 0 through 5");
        }
        while(ros::ok()) {
            ros::spinOnce();
            checkForExtract();
            int solved = checkCubeReady();
            if(solved == 1) {
                std::cout << getSolutionString() << std::endl;
                clearCubeState();
            } else if(solved == -1) {
                clearCubeState();
            }

        }
    }

}

int ColorParser::getch()
{
  static struct termios oldt, newt;
  tcgetattr( STDIN_FILENO, &oldt);           // save old settings
  newt = oldt;
  newt.c_lflag &= ~(ICANON);                 // disable buffering
  newt.c_cc[VMIN] = 0; newt.c_cc[VTIME] = 0;
  tcsetattr( STDIN_FILENO, TCSANOW, &newt);  // apply new settings

  int c = getchar();  // read character (non-blocking)

  tcsetattr( STDIN_FILENO, TCSANOW, &oldt);  // restore old settings
  return c;
}

void ColorParser::imgCallback(const cube_track::CubeTrack& msg) {
    cv_bridge::CvImagePtr cv_img = cv_bridge::toCvCopy(msg.img, sensor_msgs::image_encodings::BGR8);
    Mat scaled_img(cv_img->image.rows/SCALE_FACTOR, cv_img->image.cols/SCALE_FACTOR, cv_img->image.type());
    Size scaled_size(scaled_img.cols, scaled_img.rows);
    resize(cv_img->image, scaled_img, scaled_size);

    for(int i = 0; i < msg.sq_positions.size(); i++) {
        circle(scaled_img, Point(msg.sq_positions[i].x, msg.sq_positions[i].y),
               (int) (msg.sq_lengths[i]/2), Scalar::all(255), 2);
    }


    imshow("Scaled", scaled_img);
    waitKey(1);
    Mat tmp;
    resize(cv_img->image, tmp, scaled_size);
    GaussianBlur(tmp, cube_img, Size(5,5), 0);
    sq_positions = msg.sq_positions;
    sq_lengths = msg.sq_lengths;
}

void ColorParser::get_neighbors(Point2i& query, std::vector<Point2i>& neighbors) {
    neighbors.clear();
    if(query.x == 0) {
        switch(query.y) {
        case 0:
            neighbors.push_back(Point2i(1,2));
            neighbors.push_back(Point2i(4,0));
            break;
        case 1:
            neighbors.push_back(Point2i(4,3));
            break;
        case 2:
            neighbors.push_back(Point2i(4,6));
            neighbors.push_back(Point2i(3,0));
            break;
        case 3:
            neighbors.push_back(Point2i(1,5));
            break;
        case 5:
            neighbors.push_back(Point2i(3,3));
            break;
        case 6:
            neighbors.push_back(Point2i(1,8));
            neighbors.push_back(Point2i(5,2));
            break;
        case 7:
            neighbors.push_back(Point2i(5,5));
            break;
        case 8:
            neighbors.push_back(Point2i(3,6));
            neighbors.push_back(Point2i(5,8));
            break;
        }
    } else if(query.x == 1) {
        switch(query.y) {
        case 0:
            neighbors.push_back(Point2i(2,2));
            neighbors.push_back(Point2i(4,2));
            break;
        case 1:
            neighbors.push_back(Point2i(4,1));
            break;
        case 2:
            neighbors.push_back(Point2i(4,0));
            neighbors.push_back(Point2i(0,0));
            break;
        case 3:
            neighbors.push_back(Point2i(2,5));
            break;
        case 5:
            neighbors.push_back(Point2i(0,3));
            break;
        case 6:
            neighbors.push_back(Point2i(2,8));
            neighbors.push_back(Point2i(5,0));
            break;
        case 7:
            neighbors.push_back(Point2i(5,1));
            break;
        case 8:
            neighbors.push_back(Point2i(0,6));
            neighbors.push_back(Point2i(5,2));
            break;
        }
    } else if(query.x == 2) {
        switch(query.y) {
        case 0:
            neighbors.push_back(Point2i(4,8));
            neighbors.push_back(Point2i(3,2));
            break;
        case 1:
            neighbors.push_back(Point2i(4,5));
            break;
        case 2:
            neighbors.push_back(Point2i(4,2));
            neighbors.push_back(Point2i(1,0));
            break;
        case 3:
            neighbors.push_back(Point2i(3,5));
            break;
        case 5:
            neighbors.push_back(Point2i(1,3));
            break;
        case 6:
            neighbors.push_back(Point2i(3,8));
            neighbors.push_back(Point2i(5,6));
            break;
        case 7:
            neighbors.push_back(Point2i(5,3));
            break;
        case 8:
            neighbors.push_back(Point2i(1,6));
            neighbors.push_back(Point2i(5,0));
            break;
        }
    }else if(query.x == 3) {
        switch(query.y) {
        case 0:
            neighbors.push_back(Point2i(4,6));
            neighbors.push_back(Point2i(0,2));
            break;
        case 1:
            neighbors.push_back(Point2i(4,7));
            break;
        case 2:
            neighbors.push_back(Point2i(4,8));
            neighbors.push_back(Point2i(2,0));
            break;
        case 3:
            neighbors.push_back(Point2i(0,5));
            break;
        case 5:
            neighbors.push_back(Point2i(2,3));
            break;
        case 6:
            neighbors.push_back(Point2i(0,8));
            neighbors.push_back(Point2i(5,8));
            break;
        case 7:
            neighbors.push_back(Point2i(5,7));
            break;
        case 8:
            neighbors.push_back(Point2i(2,6));
            neighbors.push_back(Point2i(5,6));
            break;
        }
    }else if(query.x == 4) {
        switch(query.y) {
        case 0:
            neighbors.push_back(Point2i(1,2));
            neighbors.push_back(Point2i(0,0));
            break;
        case 1:
            neighbors.push_back(Point2i(1,1));
            break;
        case 2:
            neighbors.push_back(Point2i(1,0));
            neighbors.push_back(Point2i(2,2));
            break;
        case 3:
            neighbors.push_back(Point2i(0,1));
            break;
        case 5:
            neighbors.push_back(Point2i(2,1));
            break;
        case 6:
            neighbors.push_back(Point2i(0,2));
            neighbors.push_back(Point2i(3,0));
            break;
        case 7:
            neighbors.push_back(Point2i(3,1));
            break;
        case 8:
            neighbors.push_back(Point2i(3,2));
            neighbors.push_back(Point2i(2,0));
            break;
        }
    }else if(query.x == 5) {
        switch(query.y) {
        case 0:
            neighbors.push_back(Point2i(1,6));
            neighbors.push_back(Point2i(2,8));
            break;
        case 1:
            neighbors.push_back(Point2i(1,7));
            break;
        case 2:
            neighbors.push_back(Point2i(1,8));
            neighbors.push_back(Point2i(0,6));
            break;
        case 3:
            neighbors.push_back(Point2i(2,7));
            break;
        case 5:
            neighbors.push_back(Point2i(0,7));
            break;
        case 6:
            neighbors.push_back(Point2i(2,6));
            neighbors.push_back(Point2i(3,8));
            break;
        case 7:
            neighbors.push_back(Point2i(3,7));
            break;
        case 8:
            neighbors.push_back(Point2i(3,6));
            neighbors.push_back(Point2i(0,8));
            break;
        }
    }
}

int main(int argc, char** argv) {
    ros::init(argc, argv, "color_parser_node");
    ros::NodeHandle node;

    std::string sub_topic("/cube_track/cube_locations");
    bool robot_order = false;
    bool offer_service = false;
    node.param("/color_parser/input_cube_topic", sub_topic, sub_topic);
    node.param("/color_parser/robot_order", robot_order, robot_order);
    node.param("/color_parser/offer_service", offer_service, offer_service);
    ColorParser color_parse(sub_topic, robot_order, offer_service, node);
    color_parse.start(node);
}
