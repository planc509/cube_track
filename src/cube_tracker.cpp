/** This code is heavily based off of a Rubik's cube interpreter
    created by Andrej Karpathy
    Website: http://cs.stanford.edu/people/karpathy/
    Repo: https://sourceforge.net/p/pyrubikcube/code/HEAD/tree/cubefinder **/

#include "cube_track/cube_tracker.h"

CubeTracker::CubeTracker(std::string& sub_topic, ros::NodeHandle& node):
    sub_topic(sub_topic), prev_detect_lines(0), hough_thresh(100), prev_face(0),
    features(0), pt(0), prev_pt(0),
    success_count(0), detected(0), undetected_num(INT_MAX), tracking(0), p0(0,0), v1(0,0), v2(0,0),
    img_sub(node.subscribe(sub_topic, 1, &CubeTracker::imgCallback, this)) {
    prev_face.push_back(Point2f(0,0));
    prev_face.push_back(Point2f(5,0));
    prev_face.push_back(Point2f(0,5));
}

CubeTracker::~CubeTracker() {

}

bool CubeTracker::areClose(Point2f& p1, Point2f& p2, double thresh) {
    return std::abs(p1.x - p2.x) < thresh && std::abs(p1.y - p2.y) < thresh;
}

Point2f CubeTracker::avgPoint(Point2f& p1, Point2f& p2) {
    return Point2f(0.5*p1.x+0.5*p2.x, 0.5*p1.y+0.5*p2.y);
}

bool CubeTracker::intersectAngle(Point2f &p1, Point2f &p2, Point2f &q1, Point2f &q2, Point2f &uab, Point2f &offset) {
    double angle_diff = (q2.y - q1.y)*(p2.x-p1.x) - (p2.y - p1.y)*(q2.x - q1.x);
    if(std::abs(angle_diff) < 0.1) {
        return false;
    }

    double ua = (q2.x-q1.x)*(p1.y-q1.y) - (q2.y-q1.y)*(p1.x-q1.x);
    double ub = (p2.x-p1.x)*(p1.y-q1.y) - (p2.y-p1.y)*(p1.x-q1.x);
    ua = ua / angle_diff;
    ub = ub / angle_diff;
    double x = p1.x+ua*(p2.x-p1.x);
    double y = p1.y+ua*(p2.y-p1.y);

    uab.x = ua;
    uab.y = ub;
    offset.x = x;
    offset.y = y;

    return true;
}

double CubeTracker::ptDistance(Point2f &p1, Point2f &p2) {
    return std::sqrt(std::pow(p1.x-p2.x,2)+std::pow(p1.y-p2.y,2));
}

double CubeTracker::compFaces(std::vector<Point2f>& f1, std::vector<Point2f>& f2) {
    double tot_dist = 0.0;

    for(int i = 0; i < f1.size(); i++) {
        double min_dist = DBL_MAX;
        for(int j = 0; j < f2.size(); j++) {
            double dist = ptDistance(f1[i], f2[j]);
            if(dist < min_dist) {
                min_dist = dist;
            }
        }
        tot_dist += min_dist;
    }

    return tot_dist / 4;

}

void CubeTracker::winded(std::vector<Point2f>& points) {
    double avg_x = 0.0;
    double avg_y = 0.0;

    for(int i = 0; i < points.size(); i++) {
        avg_x += points[i].x;
        avg_y += points[i].y;
    }
    avg_x = avg_x/points.size();
    avg_y = avg_y/points.size();

    std::vector<double> angles;
    for(int i = 0; i < points.size(); i++) {
        double angle = std::atan2(points[i].y-avg_y, points[i].x-avg_x);
        angles.push_back(angle);
    }

    for(int i = 0; i < points.size(); i++) {
        for(int j = i+1; j < points.size(); j++) {
            if(angles[i] < angles[j]) {
                double tmp_angle = angles[i];
                Point2f tmp_point = points[i];
                angles[i] = angles[j];
                points[i] = points[j];
                angles[j] = tmp_angle;
                points[j] = tmp_point;
            }
        }
    }
}

void CubeTracker::imgCallback(const sensor_msgs::Image& msg) {
    cv_bridge::CvImagePtr cv_img = cv_bridge::toCvCopy(msg);
    Mat scaled_img(cv_img->image.rows/SCALE_FACTOR, cv_img->image.cols/SCALE_FACTOR, cv_img->image.type());
    Mat scaled_img_original(scaled_img.rows, scaled_img.cols, cv_img->image.type());
    Mat track_img(scaled_img.rows, scaled_img.cols, cv_img->image.type());
    Size scaled_size(scaled_img.cols, scaled_img.rows);
    Mat grey_img(scaled_img.rows, scaled_img.cols, CV_8UC1);
    Mat grey_threshold(scaled_img.rows, scaled_img.cols, CV_8UC1);
    Mat smoothed_img(scaled_img.rows, scaled_img.cols, CV_8UC1);
    Mat laplace_img(scaled_img.rows, scaled_img.cols, IPL_DEPTH_16S, 1);
    Mat laplace_threshold(scaled_img.rows, scaled_img.cols, CV_8UC1);
    Mat laplace_smoothed(scaled_img.rows, scaled_img.cols, CV_8UC1);

    imshow("Original", cv_img->image);
    waitKey(1);
    imshow("Scaled", scaled_img);
    waitKey(1);

    resize(cv_img->image, scaled_img, scaled_size);
    scaled_img.copyTo(scaled_img_original);
    scaled_img.copyTo(track_img);
    cvtColor(scaled_img, grey_img, CV_RGB2GRAY);

    if(tracking > 0) {

        detected = 2;
//        ROS_INFO("Drawing previous");
//        imshow("prev_grey", grey_img);
//        waitKey(0);

//        std::cout << "Old Features: ";
//        for(int i = 0; i < features.size(); i++) {
//            std::cout << "(" << features[i].x << "," << features[i].y << ")";
//        }
//        std::cout << std::endl;
        cv::vector<Point2f> new_features;
        cv::vector<Point2f> tmp_features;
        for(int i = 0; i < features.size(); i++) {
            Point2f tmp(features[i].x, features[i].y);
            tmp_features.push_back(tmp);
            new_features.push_back(tmp);
        }
        cv::vector<uchar> status;
        cv::vector<float> error;
        Size win_size(5,5);
        int max_level = 3;

    //    std::cout << "Features.size() = " << tmp_features.size() << std::endl;
        TermCriteria termcrit(TermCriteria::COUNT|TermCriteria::EPS,20,0.03);
 //       calcOpticalFlowPyrLK(prev_grey, grey_img, tmp_features, new_features, status, error,
 //                            win_size, max_level, termcrit);
        calcOpticalFlowPyrLK(prev_grey, grey_img, tmp_features, new_features, status, error);
//        std::cout << "New Features: ";
//        for(int i = 0; i < new_features.size(); i++) {
//            std::cout << "(" << new_features[i].x << "," << new_features[i].y << ")";
//        }
//        std::cout << std::endl << std::endl;

        features.clear();
        for(int i = 0; i < new_features.size(); i++) {
            if(status[i]) {
                Point2f tmp(new_features[i].x, new_features[i].y);
                features.push_back(tmp);
            }
        }
        if(features.size() < 4) {
            tracking = 0;
        } else {
            double dist1 = ptDistance(features[0], features[1]);
            double dist2 = ptDistance(features[2], features[3]);

            if(std::max(dist1,dist2)/std::min(dist1,dist2)>1.4) {
                tracking = 0;
            }

            double dist3 = ptDistance(features[0], features[2]);
            double dist4 = ptDistance(features[1], features[3]);
            if(std::max(dist3,dist4)/std::min(dist3,dist4) > 1.4) {
                tracking = 0;
            }

            if(dist1 < 10 || dist2 < 10 || dist3 < 10 || dist4 < 10) {
                tracking = 0;
            }

            if(tracking == 0) {
                detected = 0;
            }
        }
    }

    if(tracking == 0) {
        detected = 0;
        GaussianBlur(grey_img, smoothed_img,Size( GAUSS_K_SIZE, GAUSS_K_SIZE ), 0);
        cv::compare(grey_img, 100, grey_threshold, CMP_LT);
        Laplacian(smoothed_img, laplace_img, CV_16S);
        cv::compare(laplace_img, 8, laplace_threshold, CMP_GT);
 //       cv::bitwise_and(grey_threshold, laplace_threshold, laplace_threshold);
        GaussianBlur(laplace_threshold, laplace_smoothed,Size( 1, 1 ), 0);
        if(prev_detect_lines > HOUGH_LINE_QUOTA) {
            hough_thresh++;
        } else if(prev_detect_lines < HOUGH_LINE_QUOTA)  {
            hough_thresh = std::max(2, hough_thresh - 1);
        }
        std::vector<Vec4i> lines;
        std::vector<double> angles;
        HoughLinesP(laplace_smoothed, lines, 1, CV_PI / 45, hough_thresh, 10, 5);
        prev_detect_lines = lines.size();
        for(size_t i = 0; i < lines.size(); i++) {
            Vec4i l = lines[i];
            line(scaled_img, Point2f(l[0], l[1]), Point2f(l[2], l[3]),Scalar(0,0,255), 3, CV_AA);
            double angle = std::atan2(l[3]-l[1], l[2]-l[0]);
            if(angle < 0) {
                angle += CV_PI;
            }
            angles.push_back(angle);
        }

        // Find lines that share a common end point
        double intersect_thresh = 10;

        std::vector<Intersection> candidates;

        for(size_t i = 0; i < lines.size(); i++) {
            Point2f p1(lines[i][0], lines[i][1]);
            Point2f p2(lines[i][2], lines[i][3]);
            for(int j = i + 1; j < lines.size(); j++) {
                Point2f q1(lines[j][0], lines[j][1]);
                Point2f q2(lines[j][2], lines[j][3]);

                double lengthP = std::sqrt(std::pow(p2.x-p1.x,2) + std::pow(p2.y-p1.y, 2));
                double lengthQ = std::sqrt(std::pow(q2.x-q1.x,2) + std::pow(q2.y-q1.y, 2));

                if(std::max(lengthP, lengthQ) / std::min(lengthP, lengthQ) > 1.3) {
                    continue;
                }

                int matched = 0;
                Intersection it;
                if(areClose(p1, q2, intersect_thresh)) {
                    Point2f avg_pt = avgPoint(p1,q2);
                    it.setIntersection( avg_pt, p2, q1, lengthP);
                    matched++;
                }
                if(areClose(p2, q2, intersect_thresh)) {
                    Point2f avg_pt = avgPoint(p2,q2);
                    it.setIntersection(avg_pt, p1, q1, lengthP);
                    matched++;
                }
                if(areClose(p1, q1, intersect_thresh)) {
                    Point2f avg_pt = avgPoint(p1,q1);
                    it.setIntersection(avg_pt, p2, q2, lengthP);
                    matched++;
                }
                if(areClose(p2, q1, intersect_thresh)) {
                    Point2f avg_pt = avgPoint(p2, q1);
                    it.setIntersection(avg_pt, q2, p1, lengthP);
                    matched++;
                }

                if(matched == 0) {
                    Point2f uab(0,0), offset(0,0);
                    bool success = intersectAngle(p1, p2, q1, q2, uab, offset);
                    if(success && uab.x > 0 && uab.x < 1 && uab.y > 0 && uab.y < 1) {
                        int in_rangeA = 0;
                        int in_rangeB = 0;

                        if(std::abs(uab.x-1.0/3) < 0.05) {
                            in_rangeA = 1;
                        }
                        if(std::abs(uab.x-2.0/3) < 0.05) {
                            in_rangeA = 2;
                        }
                        if(std::abs(uab.y-1.0/3) < 0.05) {
                            in_rangeB = 1;
                        }
                        if(std::abs(uab.y-2.0/3) < 0.05) {
                            in_rangeB = 2;
                        }

                        if(in_rangeA > 0 &&& in_rangeB > 0) {
                            Point2f tmp(0,0);
                            if(in_rangeA == 2) {
                                tmp.x = p1.x;
                                tmp.y = p1.y;
                                p1.x = p2.x;
                                p1.y = p2.y;
                                p2.x = tmp.x;
                                p2.y = tmp.y;
                            }
                            if(in_rangeB == 2) {
                                tmp.x = q1.x;
                                tmp.y = q1.y;
                                q1.x = q2.x;
                                q1.y = q2.y;
                                q2.x = tmp.x;
                                q2.y = tmp.y;
                            }

                            Point2f z(0,0), z1(0,0), z2(0,0);
                            z1.x = q1.x + 2.0/3*(p2.x-p1.x);
                            z1.y = q1.y + 2.0/3*(p2.y-p1.y);
                            z2.x = p1.x + 2.0/3*(q2.x-q1.x);
                            z2.y = p1.y + 2.0/3*(q2.y-q1.y);
                            z.x  = p1.x - 1.0/3*(q2.x-q1.x);
                            z.y  = p1.y - 1.0/3*(q2.y-q1.y);
                            it.setIntersection(z, z1, z2, lengthP);
                            matched = 1;
                        }
                    }
                }

                if(matched == 1) {
                    double angle1 = std::atan2(p2.y - p1.y, p2.x - p1.x);
                    double angle2 = std::atan2(q2.y - q1.y, q2.x - q1.x);
                    if(angle1 < 0) {
                        angle1 += CV_PI;
                    }
                    if(angle2 < 0) {
                        angle2 += CV_PI;
                    }

                    double angle = std::abs(std::abs(angle2-angle1)-CV_PI/2.0);
                    if(angle < 0.5) {
                        candidates.push_back(it);
                    }
                }
            }
        }

        std::vector<EvidenceBundle> results;
        for(int i = 0; i < candidates.size(); i++) {
            Point2f avg_p(candidates[i].avg_p.x, candidates[i].avg_p.y);
            Point2f p1(candidates[i].p1.x, candidates[i].p1.y);
            Point2f p2(candidates[i].p2.x, candidates[i].p2.y);
            double lengthP = candidates[i].length;

            double angle1 = std::atan2(p1.y-avg_p.y, p1.x-avg_p.x);
            double angle2 = std::atan2(p2.y-avg_p.y, p2.x-avg_p.x);

            if(angle1 < 0) {
                angle1 += CV_PI;
            }
            if(angle2 < 0) {
                angle2 += CV_PI;
            }
            lengthP = 1.7*lengthP;
            int evidence = 0;
            Eigen::Matrix3d A;
            A << p2.x-avg_p.x, p1.x-avg_p.x, avg_p.x,
                 p2.y-avg_p.y, p1.y-avg_p.y, avg_p.y,
                 0,            0,            1;
            Eigen::Matrix3d A_inv = A.inverse();
            Eigen::Vector3d v;
            Eigen::Vector3d vp;
            v << p1.x, p1.y, 1;


            for(int j = 0; j < lines.size(); j++) {
                double angle = angles[j];
                double delta_a1 = std::abs(std::abs(angle-angle1)-CV_PI/2.0);
                double delta_a2 = std::abs(std::abs(angle-angle2)-CV_PI/2.0);
                if(delta_a1 > 0.1 && delta_a2 > 0.1) {
                    continue;
                }

                Point2f q1(lines[j][0], lines[j][1]);
                Point2f q2(lines[j][2], lines[j][3]);
                double qwe = 0.06;

                v.head(3) << q1.x, q1.y, 1;
                vp = A_inv*v;
                if(vp(0) > 1.1 || vp(0) < -0.1) {
                    continue;
                }
                if(vp(1) > 1.1 || vp(1) < -0.1) {
                    continue;
                }
                if(std::abs(vp(0)-1.0/3) > qwe && std::abs(vp(0)-2.0/3) > qwe &&
                   std::abs(vp(1)-1.0/3) > qwe && std::abs(vp(1)-2.0/3) > qwe) {
                    continue;
                }

                v.head(3) << q2.x, q2.y, 1;
                vp = A_inv*v;
                if(vp(0) > 1.1 || vp(0) < -0.1) {
                    continue;
                }
                if(vp(1) > 1.1 || vp(1) < -0.1) {
                    continue;
                }
                if(std::abs(vp(0)-1.0/3) > qwe && std::abs(vp(0)-2.0/3) > qwe &&
                   std::abs(vp(1)-1.0/3) > qwe && std::abs(vp(1)-2.0/3) > qwe) {
                    continue;
                }
                evidence += 1;
            }
            results.push_back(EvidenceBundle(evidence, avg_p, p1, p2));
        }

        double minch = DBL_MAX;
        std::sort(results.begin(), results.end(), std::greater<EvidenceBundle>());
        if(results.size() > 0) {
            std::vector<Point2f> minps;
            pt.clear();
            for(int i = 0; i < results.size(); i++) {
                if(results[i].evidence > 0.05*HOUGH_LINE_QUOTA) {
                    Point2f avg_p(results[i].avg_p.x, results[i].avg_p.y);
                    Point2f p1(results[i].p1.x, results[i].p1.y);
                    Point2f p2(results[i].p2.x, results[i].p2.y);
                    Point2f p3(p2.x+p1.x-avg_p.x, p2.y+p1.y-avg_p.y);
                    std::vector<Point2f> cur_points;
                    cur_points.push_back(avg_p);
                    cur_points.push_back(p1);
                    cur_points.push_back(p2);
                    cur_points.push_back(p3);

                    std::vector<Point2f> prev_points;
                    p3.x = prev_face[2].x+prev_face[1].x-prev_face[0].x;
                    p3.y = prev_face[2].y+prev_face[1].y-prev_face[0].y;
                    prev_points.push_back(prev_face[0]);
                    prev_points.push_back(prev_face[1]);
                    prev_points.push_back(prev_face[2]);
                    prev_points.push_back(p3);

                    double ch = compFaces(cur_points, prev_points);
                    if(ch < minch) {
                        minch = ch;
                        minps.clear();
                        minps.push_back(avg_p);
                        minps.push_back(p1);
                        minps.push_back(p2);
                    }
                }
            }
            if(minps.size() > 0) {
                prev_face.clear();
                for(int i = 0; i < minps.size(); i++) {
                    prev_face.push_back(minps[i]);
                }

                if(minch < 10) {
                    success_count++;
                    pt.clear();
                    for(int i = 0; i < prev_face.size(); i++) {
                        pt.push_back(prev_face[i]);
                    }
                    detected = 1;

                }
            }else {
                success_count = 0;
            }

            if(success_count > 2) {
                pt.clear();
                features.clear();
                for(int i = 1; i < 3; i++) {
                    for(int  j = 1; j < 3; j++) {
                        Point2f tmp;
                        tmp.x = p0.x+(1.0/3)*i*v1.x+(1.0/3)*j*v2.x;
                        tmp.y = p0.y+(1.0/3)*i*v1.y+(1.0/3)*j*v2.y;
                        pt.push_back(tmp);
                        features.push_back(tmp);
                    }
                }
                tracking = 1;
                success_count = 0;
            }
        }

        imshow("Laplacian", laplace_smoothed);
        waitKey(1);
    } else {
        Point2f p(features[0].x, features[0].y);
        Point2f p1(features[1].x, features[1].y);
        Point2f p2(features[2].x, features[2].y);

        v1.x = p1.x-p.x;
        v1.y = p1.y-p.y;
        v2.x = p2.x-p.x;
        v2.y = p2.y-p.y;

        pt.clear();
        prev_face.clear();
        Point2f tmp;
        tmp.x = p.x-v1.x-v2.x;
        tmp.y = p.y-v1.y-v2.y;
        pt.push_back(tmp);
        prev_face.push_back(tmp);
        tmp.x = p.x+2*v2.x-v1.x;
        tmp.y = p.y+2*v2.y-v1.y;
        pt.push_back(tmp);
        prev_face.push_back(tmp);
        tmp.x = p.x+2*v1.x-v2.x;
        tmp.y = p.y+2*v1.y-v2.y;
        pt.push_back(tmp);
        prev_face.push_back(tmp);
    }

    if(detected || undetected_num < 1) {
        if( !detected) {
            undetected_num += 1;
            pt.clear();
            for(int i = 0; i < prev_pt.size(); i++) {
                pt.push_back(prev_pt[i]);
            }
        }

        if(detected) {
            undetected_num = 0;
            prev_pt.clear();
            for(int i = 0; i < pt.size(); i++) {
                prev_pt.push_back(pt[i]);
            }
        }

        Mat hsv(scaled_img.rows, scaled_img.cols, CV_8UC3);
        std::vector<Mat> hsv_comps(3);
        cvtColor(scaled_img_original, hsv, CV_RGB2HSV);
        split(hsv, hsv_comps);

        Point2f p3(pt[2].x+pt[1].x-pt[0].x, pt[2].y+pt[1].y-pt[0].y);
        line(track_img, pt[0], pt[1], Scalar(0,255,0), 2);
        line(track_img, pt[1], p3, Scalar(0,255,0), 2);
        line(track_img, p3, pt[2], Scalar(0,255,0), 2);
        line(track_img, pt[2], pt[0], Scalar(0,255,0), 2);

        pt.push_back(p3);
        winded(pt);

        v1.x = pt[1].x-pt[0].x;
        v1.y = pt[1].y-pt[0].y;
        v2.x = pt[3].x-pt[0].x;
        v2.y = pt[3].y-pt[0].y;
        p0.x = pt[0].x;
        p0.y = pt[0].y;

        std::vector<Point2f> end_pts;
        std::vector<Point2f> mid_pts;
        int i = 1;
        int j  = 5;

        Point2f tmp;
        for(int k = 0; k < 9; k++) {
            tmp.x = p0.x+i*v1.x/6.0+j*v2.x/6.0;
            tmp.y = p0.y+i*v1.y/6.0+j*v2.y/6.0;
            end_pts.push_back(tmp);
            i += 2;
            if(i == 7) {
                i = 1;
                j -= 2;
            }
        }
        tmp.x = 0.0;
        tmp.y = 0.0;
        double rad = ptDistance(v1, tmp)/6.0;
        imshow("Track", track_img);
        waitKey(1);
        swap(prev_grey, grey_img);
    }
}

Intersection::Intersection() :
    avg_p(0,0), p1(0,0), p2(0,0), length(0) {

}

Intersection::Intersection(Point2f avg_p, Point2f p1, Point2f p2, double length) :
    avg_p(avg_p.x, avg_p.y), p1(p1.x, p1.y), p2(p2.x, p2.y), length(0) {

}

Intersection::~Intersection() {

}

EvidenceBundle::EvidenceBundle(): evidence(0), avg_p(0,0), p1(0,0), p2(0,0) {

}

EvidenceBundle::EvidenceBundle(int evidence, Point2f avg_p, Point2f p1, Point2f p2):
    evidence(evidence), avg_p(avg_p.x, avg_p.y), p1(p1.x, p1.y), p2(p2.x, p2.y) {

}

EvidenceBundle::~EvidenceBundle() {

}

bool EvidenceBundle::operator > (const EvidenceBundle& evidence_bundle) const {
    return (this->evidence > evidence_bundle.evidence);
}

bool EvidenceBundle::operator < (const EvidenceBundle& evidence_bundle) const {
    return (this->evidence < evidence_bundle.evidence);
}

void Intersection::setIntersection(Point2f& avg_p, Point2f& p1, Point2f& p2, double length) {
    this->avg_p.x = avg_p.x;
    this->avg_p.y = avg_p.y;
    this->p1.x = p1.x;
    this->p1.y = p1.y;
    this->p2.x = p2.x;
    this->p2.y = p2.y;
    this->length = length;
}

int main(int argc, char** argv) {
    ros::init(argc, argv, "cube_tracker");
    ros::NodeHandle node;

    std::string sub_topic("/camera/rgb/image_raw");
    node.param("/cube_tracker/img_input_topic", sub_topic, sub_topic);
    CubeTracker cube_track(sub_topic, node);
    ros::spin();
}
