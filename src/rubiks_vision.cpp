#include "cube_track/rubiks_vision.h"

RubiksVision::RubiksVision(std::string &sub_topic, std::string& pub_topic,
                           std::string& camera_info,
                           std::string& camera_link, ros::NodeHandle &node) :
    pub_topic(pub_topic),
    sub_topic(sub_topic), camera_info(camera_info), camera_link(camera_link),
    point_head("/head_traj_controller/point_head_action", true), prev_detect_lines(0),
    hough_thresh(200),
    img_sub(node.subscribe(sub_topic, 1, &RubiksVision::imgCallback, this)),
    cube_pub(node.advertise<cube_track::CubeTrack>(pub_topic,1)){

    sensor_msgs::CameraInfo::ConstPtr info = ros::topic::waitForMessage<sensor_msgs::CameraInfo>(camera_info, node);
    coord_to_pixel << info->K[0], info->K[1], info->K[2],
                      info->K[3], info->K[4], info->K[5],
                      info->K[6], info->K[7], info->K[8];
//    while(!point_head.waitForServer(ros::Duration(5.0)));

}

RubiksVision::~RubiksVision() {

}

void RubiksVision::pointHead(double x, double y, double z, std::string &target_frame, std::string &ref_frame) {

    pr2_controllers_msgs::PointHeadGoal goal;
    geometry_msgs::PointStamped point;
    point.header.frame_id = target_frame;
    point.point.x = x;
    point.point.y = y;
    point.point.z = z;
    goal.target = point;

    goal.pointing_frame = ref_frame;
    //goal.min_duration = ros::Duration(0.5);
    goal.max_velocity = 1.0;

    point_head.sendGoal(goal);
    //point_head.waitForResult(ros::Duration(2.0));

}

HoldingGripper RubiksVision::getHoldingGripper(void) {
    tf::StampedTransform grip_width_right;

    double width = 0.0;
    tf_listener.lookupTransform("r_gripper_l_finger_tip_link",
                                "r_gripper_r_finger_tip_link",
                                ros::Time(0),
                                grip_width_right);
    width = std::abs(grip_width_right.getOrigin().getY());
    if(width < 1.1*CUBE_GRIP_WIDTH && width > 0.9*CUBE_GRIP_WIDTH) {
        return RIGHT_GRIPPER;
    }

    tf::StampedTransform grip_width_left;
    tf_listener.lookupTransform("l_gripper_l_finger_tip_link",
                                "l_gripper_r_finger_tip_link",
                                ros::Time(0),
                                grip_width_left);
    width = std::abs(grip_width_left.getOrigin().getY());
    if(width < 1.1*CUBE_GRIP_WIDTH && width > 0.9*CUBE_GRIP_WIDTH) {
        return LEFT_GRIPPER;
    }

    return NONE_GRIPPER;
}

Point2f RubiksVision::getImgCoord(std::string& link) {

    Eigen::Vector3d camera_coords;
    Eigen::Vector3d screen_coords;

    tf::StampedTransform link_transform;
    tf_listener.lookupTransform(camera_link,
                                link,
                                ros::Time(0),
                                link_transform);
    camera_coords(0) = link_transform.getOrigin().getX();
    camera_coords(1) = link_transform.getOrigin().getY();
    camera_coords(2) = link_transform.getOrigin().getZ();
    screen_coords = coord_to_pixel*camera_coords;

    return Point2f((screen_coords(0)/screen_coords(2)), (screen_coords(1)/screen_coords(2)));
}

Point2f RubiksVision::getImgCoord(std::string &link, double x_offset, double y_offset, double z_offset) {
    Eigen::Vector3d camera_coords;
    Eigen::Vector3d screen_coords;

		ros::Time now = ros::Time::now();

    geometry_msgs::PoseStamped pose_in, pose_out;
    pose_in.header.frame_id = link;
    pose_in.header.stamp = now;
    pose_in.pose.position.x = x_offset;
    pose_in.pose.position.y = y_offset;
    pose_in.pose.position.z = z_offset;
    pose_in.pose.orientation.w = 1.0;
    
    
    tf_listener.waitForTransform(link, camera_link, now, ros::Duration(3.0));
    tf_listener.transformPose(camera_link, now, pose_in, link, pose_out);
    camera_coords(0) = pose_out.pose.position.x;
    camera_coords(1) = pose_out.pose.position.y;
    camera_coords(2) = pose_out.pose.position.z;
    screen_coords = coord_to_pixel*camera_coords;

    return Point2f((screen_coords(0)/screen_coords(2)), (screen_coords(1)/screen_coords(2)));
}

void RubiksVision::blackoutRegion(Mat &img, double slope, double offset1, double offset2, bool vertical) {

    Vec3b black;
    black[0] = 0;
    black[1] = 0;
    black[2] = 0;
    if(!vertical) {
        for(int i = 0; i < img.cols; i++) {
            for(int j = 0; j < img.rows; j++) {
                double y1 = slope*i+offset1;
                double y2 = slope*i+offset2;
                if((j > y1 && j > y2) || (j < y1 && j < y2)) {
                    img.at<Vec3b>(Point(i,j)) = black;
                }
            }
        }
    } else {
        for(int i = 0; i < img.cols; i++) {
            for(int j = 0; j < img.rows; j++) {
                if((i > offset1 && i > offset2) || (i < offset1 && i < offset2)) {
                    img.at<Vec3b>(Point(i,j)) = black;
                }
            }
        }
    }
}

void RubiksVision::blackoutCircle(Mat &img, Point2f &center, int radius) {
    Vec3b black;
    black[0] = 0;
    black[1] = 0;
    black[2] = 0;
    for(int i = 0; i < img.cols; i++) {
        for(int j = 0; j < img.rows; j++) {
            Point2f tmp(i,j);
            double dist = ptDist(center, tmp);
            if(dist > radius) {
                img.at<Vec3b>(Point(i,j)) = black;
            }
        }
    }
}

double RubiksVision::ptDist(Point2f& p1, Point2f& p2) {
    return std::sqrt(std::pow(p1.x-p2.x,2)+std::pow(p1.y-p2.y,2));
}

std::vector<Vec4i> RubiksVision::findLines(Mat &img, int min_length) {
    Mat grey(img.rows, img.cols, CV_8UC1);
    Mat smoothed_grey(img.rows, img.cols, CV_8UC1);
    Mat laplace_img(img.rows, img.cols, IPL_DEPTH_16S, 1);
    Mat laplace_threshold(img.rows, img.cols, CV_8UC1);

    cvtColor(img, grey, CV_RGB2GRAY);
    GaussianBlur(grey, smoothed_grey, Size(1, 1), 0);
    Laplacian(smoothed_grey, laplace_img, CV_16S);
    cv::compare(laplace_img, 8, laplace_threshold, CMP_GT);

    if(prev_detect_lines > HOUGH_LINE_QUOTA) {
        hough_thresh++;
    } else if(prev_detect_lines < HOUGH_LINE_QUOTA) {
        hough_thresh = std::max(2, hough_thresh-1);
    }

    std::vector<Vec4i> lines;
    HoughLinesP(laplace_threshold, lines, 1, CV_PI/45, hough_thresh, min_length, 10);
    prev_detect_lines = lines.size();

    return lines;

}

std::vector<Vec4i> RubiksVision::findCenterSquare(std::vector<Vec4i>& lines) {
    std::vector<double> candidate_angles;
    std::vector<Vec4i> candidate_lines;
    double angle = 0.0;
    for(int i = 0; i < lines.size(); i++) {
        for(int j = i+1; j < lines.size(); j++) {
            for(int k = j+1; k < lines.size(); k++) {
                for(int l = k+1; l < lines.size(); l++) {
                    int normal_count = 0;
                    int parallel_count = 0;
                    candidate_lines.clear();
                    candidate_angles.clear();
                    candidate_lines.push_back(lines[i]);
                    candidate_lines.push_back(lines[j]);
                    candidate_lines.push_back(lines[k]);
                    candidate_lines.push_back(lines[l]);
                    for(int m = 0; m < candidate_lines.size(); m++) {
                        angle = std::atan2(candidate_lines[m][3]-candidate_lines[m][1], candidate_lines[m][2]-candidate_lines[m][0]);
                        if(angle < 0) {
                            angle += CV_PI;
                        }
                        candidate_angles.push_back(angle);
                    }

                    bool exit = false;
                    for(int m = 0; m < candidate_angles.size(); m++) {
                        for(int n = m+1; n < candidate_angles.size(); n++) {
                            double diff = std::abs(candidate_angles[m]-candidate_angles[n]);
                            if(diff < 0.1*CV_PI) {
                                parallel_count++;
                            } else if(diff > (CV_PI/2)-0.1*CV_PI && diff < (CV_PI/2)+0.1*CV_PI) {
                                if(intersect(candidate_lines[m], candidate_lines[n])) {
                                    normal_count++;
                                } else {
                                    exit = true;
                                    break;
                                }
                            }else{
                                exit = true;
                                break;
                            }
                        }
                        if(exit) {
                            break;
                        }
                    }
                    if(parallel_count == 2 && normal_count == 4) {
                        // We found it
                        return candidate_lines;
                    }
                }
            }
        }
    }
    candidate_lines.clear();
    return candidate_lines;
}

bool RubiksVision::intersect(Vec4i& l1, Vec4i& l2) {
    // First see if they connect at end points;
    double gap = 10;
    for(int i = 0; i < 4; i+=2) {
        for(int j = 0; j < 4; j+=2) {
            double dist = std::sqrt(std::pow(l1[i]-l2[j],2)+std::pow(l1[i+1]-l2[j+1],2));
            if(dist < gap) {
                return true;
            }
        }
    }
    Point2f mid_point((l1[0]+l1[2])/2.0, (l1[1]+l1[3])/2.0);
    if((l2[0] > mid_point.x-gap && l2[2] < mid_point.x+gap) || (l2[2] > mid_point.x-gap && l2[0] < mid_point.x+gap)) {
        if((l2[1] > mid_point.y-gap && l2[3] < mid_point.y+gap) || (l2[3] > mid_point.y-gap && l2[1] < mid_point.y+gap)) {
            return true;
        }
    }
    return false;
}

double RubiksVision::findMatch(Mat& img, Mat& template_img, Point& match_loc) {
    Mat result(img.rows-template_img.rows+1, img.cols-template_img.cols+1, CV_32FC1);
    matchTemplate(img, template_img, result, CV_TM_CCORR_NORMED);
//    normalize(result, result, 0, 1, NORM_MINMAX, -1, Mat());
    double minVal; double maxVal; Point minLoc; Point maxLoc;
    minMaxLoc( result, &minVal, &maxVal, &minLoc, &maxLoc, Mat() );
    match_loc = maxLoc;
    return maxVal;
}

Point2f RubiksVision::transformPoint(Point2f& p, double angle, Point2f& offset) {
    double cos_val = std::cos(angle);
    double sin_val = std::sin(angle);
    Point rotated(p.x*cos_val-p.y*sin_val,p.x*sin_val+p.y*cos_val);
    return Point(rotated.x+offset.x,rotated.y+offset.y);
}

void RubiksVision::imgCallback(const sensor_msgs::Image &msg) {
    cv_bridge::CvImagePtr cv_img = cv_bridge::toCvCopy(msg);
    Mat scaled_img(cv_img->image.rows/SCALE_FACTOR, cv_img->image.cols/SCALE_FACTOR, cv_img->image.type());
    Mat grey(scaled_img.rows, scaled_img.cols, CV_8UC1);
    Size scaled_size(scaled_img.cols, scaled_img.rows);
    resize(cv_img->image, scaled_img, scaled_size);
    cvtColor(scaled_img, grey, CV_RGB2GRAY);
    Mat hough_img(scaled_img.cols, scaled_img.rows, cv_img->image.type());
    Mat center_square_img(scaled_img.cols, scaled_img.rows, cv_img->image.type());
    scaled_img.copyTo(hough_img);
    scaled_img.copyTo(center_square_img);
//    imshow("Scaled", scaled_img);
//    waitKey(1);

    // Figure out which hand is holding the cube
    HoldingGripper holding_gripper = getHoldingGripper();
    std::string grip_prefix("");
    if(holding_gripper == LEFT_GRIPPER) {
        grip_prefix = "l";
    } else if(holding_gripper == RIGHT_GRIPPER) {
        grip_prefix = "r";
    } else {
        ROS_INFO_ONCE("No gripper holding");
        return;
    }

    std::string tool_frame(grip_prefix+"_gripper_tool_frame");
//    pointHead(0,0,0, tool_frame, camera_link);

    std::string r_finger_link(grip_prefix+"_gripper_r_finger_tip_link");
    std::string l_finger_link(grip_prefix+"_gripper_l_finger_tip_link");
    Point2f r_finger = getImgCoord(r_finger_link, 0.03, R_FINGER_OFFSET, 0);
    Point2f l_finger = getImgCoord(l_finger_link, 0.03, L_FINGER_OFFSET, 0);
    double slope = 0.0;
    double r_offset = 0.0;
    double l_offset = 0.0;

    Point scaled_r_finger((int)(r_finger.x/SCALE_FACTOR), (int)(r_finger.y/SCALE_FACTOR));
    Point scaled_l_finger((int)(l_finger.x/SCALE_FACTOR), (int)(l_finger.y/SCALE_FACTOR));

    if(r_finger.x > 0 && r_finger.y > 0 && r_finger.x < scaled_img.cols && r_finger.y < scaled_img.rows &&
       l_finger.x > 0 && l_finger.y > 0 && l_finger.x < scaled_img.cols && l_finger.y < scaled_img.rows) {
        Point2f center((r_finger.x+l_finger.x)/2.0, (r_finger.y+l_finger.y)/2.0);
        double radius = ptDist(r_finger, l_finger);
        radius = 1.15*radius/2;
        Mat cropped(2*radius+1, 2*radius+1, CV_8UC1);
        for(int i = -radius; i <= radius; i++) {
            for(int j = -radius; j <= radius; j++) {
                cropped.at<uchar>(Point(radius+i,radius+j)) = grey.at<uchar>(Point(center.x+i, center.y+j));
            }
        }
        Mat cropped_smoothed(cropped.rows, cropped.cols, CV_8UC1);
        Mat laplace_img(cropped.rows, cropped.cols, IPL_DEPTH_16S, 1);
        Mat laplace_threshold(cropped.rows, cropped.cols, CV_8UC1);
        GaussianBlur(cropped, cropped_smoothed, Size(5, 5), 0);
        Laplacian(cropped_smoothed, laplace_img, CV_16S);
        cv::compare(laplace_img, 4, laplace_threshold, CMP_GT);

//        blackoutCircle(scaled_img, center, radius);
     //   line(scaled_img, scaled_r_finger, scaled_l_finger, Scalar(0,255,0));
//        imshow("Track", scaled_img);
//        waitKey(1);

        double length = 0.7*std::sqrt(std::pow(scaled_r_finger.x-scaled_l_finger.x, 2)+std::pow(scaled_r_finger.y-scaled_l_finger.y,2));
        Mat template_img(length, length, CV_8UC1, Scalar(0));

        line(template_img, Point(0,length/3), Point(length-1,length/3), Scalar(255), 5);
        line(template_img, Point(0,2*length/3), Point(length-1,2*length/3), Scalar(255),5);
        line(template_img, Point(length/3,0), Point(length/3, length-1), Scalar(255),5);
        line(template_img, Point(2*length/3,0), Point(2*length/3, length-1), Scalar(255),5);

        Point matchLoc;
        double max_score = -DBL_MAX;
        Mat template_warp;
        Point2f src_center(template_img.cols/2.0F, template_img.rows/2.0F);
        double min_angle = -1*180/6;
        double max_angle = 180/6;
        double angle = min_angle;
        double best_angle = 0.0;
        Mat rot_mat = getRotationMatrix2D(src_center, angle, 1.0);
        if(template_img.cols <= 0 || template_img.rows <= 0) {
            return;
        }
        warpAffine(template_img, template_warp, rot_mat, template_img.size());
        max_score = findMatch(laplace_threshold, template_warp, matchLoc);
//        imshow("warp", template_warp);
//        waitKey(1);
        while(angle < max_angle) {
            Point candidate_loc;
            rot_mat = getRotationMatrix2D(src_center, angle, 1.0);
            warpAffine(template_img, template_warp, rot_mat, template_img.size());
            double score = findMatch(laplace_threshold, template_warp, candidate_loc);
            if(score > max_score) {
                max_score = score;
                matchLoc = candidate_loc;
                best_angle = angle;
            }
            angle += 0.05*(max_angle-min_angle);
        }
        best_angle = -1*best_angle * CV_PI/180;
        Point2f offset(center.x-radius+matchLoc.x+template_img.cols/2.0,
                     center.y-radius+matchLoc.y+template_img.rows/2.0);
        Point2f w1(-1*template_img.cols/2.0,-1*template_img.rows/2.0);
        Point2f w2(-1*template_img.cols/2.0,   template_img.rows/2.0);
        Point2f w3(   template_img.cols/2.0,   template_img.rows/2.0);
        Point2f w4(   template_img.cols/2.0,-1*template_img.rows/2.0);
        Point2f p1 = transformPoint(w1, best_angle, offset);
        Point2f p2 = transformPoint(w2, best_angle, offset);
        Point2f p3 = transformPoint(w3, best_angle, offset);
        Point2f p4 = transformPoint(w4, best_angle, offset);
        line(scaled_img, p1,p2, Scalar(0,255,0),2,8,0);
        line(scaled_img, p2,p3, Scalar(0,255,0),2,8,0);
        line(scaled_img, p3,p4, Scalar(0,255,0),2,8,0);
        line(scaled_img, p1,p4, Scalar(0,255,0),2,8,0);
        imshow("Tracked", scaled_img);
        waitKey(1);

        cube_track::CubeTrack cube_msg;
        cube_msg.header.frame_id = camera_link;
        cube_msg.header.stamp = ros::Time::now();

        Point2f tmp;
        for(int j = -1; j <= 1; j++) {
            for(int i = -1; i <= 1; i++) {
                tmp.x = i*template_img.cols/3.0;
                tmp.y = j*template_img.rows/3.0;
                tmp = transformPoint(tmp, best_angle, offset);
                geometry_msgs::Point p;
                p.x = (int) (tmp.x+0.5);
                p.y = (int) (tmp.y+0.5);
                cube_msg.sq_positions.push_back(p);
                cube_msg.sq_lengths.push_back(0.55*length/2);
            }
        }
        cube_msg.img = msg;
        cube_pub.publish(cube_msg);
                /*        double sin_val = std::sin(best_angle);
                        double cos_val = std::cos(best_angle);
                        // warped template to template
                        Point2f w1((-1*template_img.cols/2.0)*cos_val-(-1*template_img.rows/2.0)*sin_val,
                                 (-1*template_img.cols/2.0)*sin_val+(-1*template_img.rows/2.0)*cos_val);
                        Point2f w2((-1*template_img.cols/2.0)*cos_val-(template_img.rows/2.0)*sin_val,
                                 (-1*template_img.cols/2.0)*sin_val+(template_img.rows/2.0)*cos_val);
                        Point2f w3((template_img.cols/2.0)*cos_val-(template_img.rows/2.0)*sin_val,
                                 (template_img.cols/2.0)*sin_val+(template_img.rows/2.0)*cos_val);
                        Point2f w4((template_img.cols/2.0)*cos_val-(-1*template_img.rows/2.0)*sin_val,
                                 (template_img.cols/2.0)*sin_val+(-1*template_img.rows/2.0)*cos_val);
                        // template to cropped
                        Point2f c1(w1.x+matchLoc.x+template_img.cols/2.0, w1.y+matchLoc.y+template_img.rows/2.0);
                        Point2f c2(w2.x+matchLoc.x+template_img.cols/2.0, w2.y+matchLoc.y+template_img.rows/2.0);
                        Point2f c3(w3.x+matchLoc.x+template_img.cols/2.0, w3.y+matchLoc.y+template_img.rows/2.0);
                        Point2f c4(w4.x+matchLoc.x+template_img.cols/2.0, w4.y+matchLoc.y+template_img.rows/2.0);

                        // Cropped to scaled_img
                        Point p1(center.x-radius+c1.x, center.y-radius+c1.y);
                        Point p2(center.x-radius+c2.x, center.y-radius+c2.y);
                        Point p3(center.x-radius+c3.x, center.y-radius+c3.y);
                        Point p4(center.x-radius+c4.x, center.y-radius+c4.y);*/
        //        rectangle( scaled_img, Point( center.x-radius+r1.x, center.y-radius+r1.y ),
        //                   Point( center.x-radius+r2.x , center.y-radius+r2.y ),
        //                   Scalar::all(255), 2, 8, 0 );


        /*
        line(hough_img, scaled_r_finger, scaled_l_finger, Scalar(0,255,0));
        double min_length = std::sqrt(std::pow(scaled_r_finger.x-scaled_l_finger.x, 2)+std::pow(scaled_r_finger.y-scaled_l_finger.y,2));
        ROS_INFO_ONCE("min_length: %f", min_length);
        std::vector<Vec4i> lines = findLines(scaled_img, 0.3*min_length);
        std::vector<Vec4i> filtered_lines;
        double thresh = (CV_PI/2);
        for(int i = 0; i < lines.size(); i++) {
            double angle1 = std::atan2(lines[i][3]-lines[i][1],lines[i][2]-lines[i][0]);
            if(angle1 < 0) {
                angle1 += CV_PI;
            }
            for(int j = 0; j < lines.size(); j++) {
                if(i != j) {
                    if(intersect(lines[i], lines[j])) {
                        double angle2 = std::atan2(lines[j][3]-lines[j][1],lines[j][2]-lines[j][0]);
                        if(angle2 < CV_PI) {
                            angle2 += CV_PI;
                        }
                        double diff = std::abs(angle1-angle2);
                        if(diff < thresh+0.05*CV_PI && diff > thresh-0.05*CV_PI) {
                            filtered_lines.push_back(lines[i]);
                            break;
                        }
                    }
                }

            }
        }

        for(size_t i = 0; i < filtered_lines.size(); i++) {
            Vec4i l = filtered_lines[i];
            line(hough_img, Point2f(l[0], l[1]), Point2f(l[2], l[3]), Scalar(0,0,255), 3, CV_AA);
        }
//        line(hough_img, scaled_r_finger, scaled_l_finger, Scalar(0,255,0));
        imshow("Hough lines", hough_img);
        waitKey(1);*/

 /*       std::vector<Vec4i> center_square = findCenterSquare(lines);
        if(!center_square.empty()) {
            for(size_t i = 0; i < center_square.size(); i++) {
                Vec4i l = center_square[i];
                line(center_square_img, Point2f(l[0], l[1]), Point2f(l[2],l[3]), Scalar(255,0,0));
            }
            imshow("Center Square", center_square_img);
            waitKey(1);
        }*/
    } else {
        //std::cout << "Out of bound Coords: ";
        //std::cout << "r_finger = (" <<r_finger.x << "," << r_finger.y << ") ";
        //std::cout << "l_finger = (" <<l_finger.x << "," << l_finger.y << ")" << std::endl<<std::endl;
    }


}

/*        if(std::abs(l_finger.y-r_finger.y) > 0) {
            slope = -1*(r_finger.x-l_finger.x)/(r_finger.y-l_finger.y);
            r_offset = r_finger.y-slope*r_finger.x;
            l_offset = l_finger.y-slope*l_finger.x;
            blackoutRegion(scaled_img, slope, r_offset, l_offset);
        } else {
            blackoutRegion(scaled_img, -1, r_finger.x, l_finger.x, true);
        }

        Point2f up = getImgCoord(tool_frame, 0.05, 0, 0);
        Point2f down = getImgCoord(tool_frame, -0.05, 0, 0);
        Point scaled_up((int)(up.x/SCALE_FACTOR), (int)(up.y/SCALE_FACTOR));
        Point scaled_down((int)(down.x/SCALE_FACTOR), (int)(down.y/SCALE_FACTOR));
        if(up.x > 0 && up.y > 0 && up.x < scaled_img.cols && up.y < scaled_img.rows &&
           down.x > 0 && down.y > 0 && down.x < scaled_img.cols && down.y < scaled_img.rows) {
            if(std::abs(l_finger.y-r_finger.y) > 0) {
                slope = (r_finger.y-l_finger.y)/(r_finger.x-l_finger.x);
                double up_offset = up.y-slope*up.x;
                double down_offset = down.y-slope*down.x;
                blackoutRegion(scaled_img, slope, up_offset, down_offset);
            } else {
                blackoutRegion(scaled_img, -1, up.x, down.x, true);
            }
        }*/

int main(int argc, char** argv) {
    ros::init(argc, argv, "rubiks_vision_node");
    ros::NodeHandle node;

    std::string sub_topic("/camera/rgb/image_raw");
    std::string pub_topic("/cube_track/cube_locations");
    std::string camera_link("head_mount_kinect_rgb_optical_frame");
    node.param("/rubiks_vision/img_input_topic", sub_topic, sub_topic);
    node.param("/rubiks_vision/cube_output_topic", pub_topic, pub_topic);
    node.param("/rubiks_vision/camera_link", camera_link, camera_link);
    std::vector<std::string> elements;
    std::stringstream ss(sub_topic);
    std::string item;
    while(std::getline(ss, item, '/')) {
        elements.push_back(item);
    }
    std::string camera_info("");
    for(int i = 0; i < elements.size()-1;i++) {
        if(!elements[i].empty()) {
            camera_info += "/"+elements[i];
        }
    }
    camera_info += "/camera_info";
    RubiksVision parse_cube(sub_topic, pub_topic, camera_info, camera_link, node);
    ros::spin();
}
