#include <ros/ros.h>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Dense>
#include "cube_track/cube_tracker.h"
#include <vector>

void winded(std::vector<Point>& points) {
    double avg_x = 0.0;
    double avg_y = 0.0;

    for(int i = 0; i < points.size(); i++) {
        avg_x += points[i].x;
        avg_y += points[i].y;
    }
    avg_x = avg_x/points.size();
    avg_y = avg_y/points.size();

    std::vector<double> angles;
    std::cout << "Angles: ";
    for(int i = 0; i < points.size(); i++) {
        double angle = std::atan2(points[i].y-avg_y, points[i].x-avg_x);
        angles.push_back(angle);
        std::cout << angle << ", ";
    }
    std::cout << std::endl;

    for(int i = 0; i < points.size(); i++) {
        for(int j = i+1; j < points.size(); j++) {
            if(angles[i] < angles[j]) {
                double tmp_angle = angles[i];
                Point tmp_point = points[i];
                angles[i] = angles[j];
                points[i] = points[j];
                angles[j] = tmp_angle;
                points[j] = tmp_point;
            }
        }
    }
}

int main(int argc, char** argv) {
    ros::init(argc, argv, "eigen_test_node");
    ros::NodeHandle node;

    Eigen::Matrix3d A;
    A << 1,2,1,
         2,1,0,
         -1,1,2;
    Eigen::Matrix3d A_inv = A.inverse();
//    std::cout << A << std::endl << std::endl;
    std::cout << A_inv << std::endl << std::endl;
//    std::cout << A*A_inv << std::endl << std::endl;

    Eigen::Vector3d V;
    V << 4, 12, 1;
    std::cout << V << std::endl << std::endl;

    V.head(3) << 7,8,9;
    std::cout << V << std::endl << std::endl;
    std::cout << A_inv * V << std::endl << std::endl;
    std::cout << V(0) << " " << V(1) << " " << V(2) << std::endl << std::endl;

    EvidenceBundle e1(6, Point(0,0), Point(0,0), Point(0,0));
    EvidenceBundle e2(1, Point(0,0), Point(0,0), Point(0,0));
    EvidenceBundle e3(18, Point(0,0), Point(0,0), Point(0,0));
    EvidenceBundle e4(2, Point(0,0), Point(0,0), Point(0,0));
    EvidenceBundle e5(9, Point(0,0), Point(0,0), Point(0,0));
    std::vector<EvidenceBundle> res;
    res.push_back(e1);
    res.push_back(e2);
    res.push_back(e3);
    res.push_back(e4);
    res.push_back(e5);

    std::sort(res.begin(), res.end(), std::greater<EvidenceBundle>());
  //  for(unsigned int i = 0; i < res.size(); i++) {
  //      std::cout << res[i].evidence << std::endl;
  //  }

    Point p1(0,1);
    Point p2(-1,0);
    Point p3(1,0);
    Point p4(0,-1);
    std::vector<Point> points;
    points.push_back(p1);
    points.push_back(p2);
    points.push_back(p3);
    points.push_back(p4);

    std::cout << "Before: ";
    for(int i = 0; i < points.size(); i++) {
        std::cout << "(" << points[i].x << "," << points[i].y << "), ";
    }
    std::cout << std::endl;

    winded(points);
    std::cout << "After: ";
    for(int i = 0; i < points.size(); i++) {
        std::cout << "(" << points[i].x << "," << points[i].y << "), ";
    }
    std::cout << std::endl;

    ros::spin();
}
