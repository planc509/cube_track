/** This code is heavily based off of a Rubik's cube interpreter
    created by Andrej Karpathy
    Website: http://cs.stanford.edu/people/karpathy/
    Repo: https://sourceforge.net/p/pyrubikcube/code/HEAD/tree/cubefinder: **/

#ifndef CUBE_TRACKER_H
#define CUBE_TRACKER_H

#define SCALE_FACTOR 2
#define GAUSS_K_SIZE 5
#define HOUGH_LINE_QUOTA 200

#include <ros/ros.h>
#include <string>
#include <vector>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/video/tracking.hpp"
#include <opencv2/imgproc/imgproc.hpp>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Dense>
#include <limits>

using namespace cv;

class Intersection {
public:
    Point2f avg_p;
    Point2f p1;
    Point2f p2;
    double length;
    Intersection();
    ~Intersection();
    Intersection(Point2f avg_p, Point2f p1, Point2f p2, double length);
    void setIntersection(Point2f& avg_p, Point2f& p1, Point2f& p2, double length);
};

class EvidenceBundle {
public:
    int evidence;
    Point2f avg_p;
    Point2f p1;
    Point2f p2;
    EvidenceBundle();
    EvidenceBundle(int evidence, Point2f avg_p, Point2f p1, Point2f p2);
    ~EvidenceBundle();
    bool operator > (const EvidenceBundle& evidence_bundle) const;
    bool operator < (const EvidenceBundle& evidence_bundle) const;
};

class CubeTracker{
private:
    int prev_detect_lines;
    int hough_thresh;
    int success_count;
    int detected;
    int undetected_num;
    int tracking;
    Point2f p0;
    Point2f v1;
    Point2f v2;
    std::vector<Point2f> prev_face;
    std::vector<Point2f> features;
    std::vector<Point2f> pt;
    std::vector<Point2f> prev_pt;
    Mat prev_grey;
    std::string sub_topic;
    ros::Subscriber img_sub;
    void imgCallback(const sensor_msgs::Image& msg);
    Point2f avgPoint(Point2f& p1, Point2f& p2);
    bool areClose(Point2f& p1, Point2f& p2, double thresh);
    bool intersectAngle(Point2f& p1, Point2f& p2, Point2f& q1, Point2f& q2, Point2f& uab, Point2f& offset);
    double ptDistance(Point2f& p1, Point2f& p2);
    double compFaces(std::vector<Point2f>& f1, std::vector<Point2f>& f2);
    void winded(std::vector<Point2f>& Point2fs);
public:
    CubeTracker(std::string& sub_topic, ros::NodeHandle& node);
    ~CubeTracker();
};

#endif // CUBE_TRACKER_H
