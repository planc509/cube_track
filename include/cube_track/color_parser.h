#ifndef COLOR_PARSER_H
#define COLOR_PARSER_H

#define SCALE_FACTOR 1
#define COLOR_SPACES 2
#define HSV_IDX 1
#define LUV_IDX 0
#define USE_RATIO_DIST 1

#include <string>
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include "cube_track/CubeTrack.h"
#include "cube_track/ExtractCube.h"
#include "cube_track/SolveCube.h"
#include <cv_bridge/cv_bridge.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include<stdio.h>
#include <termios.h>            //termios, TCSANOW, ECHO, ICANON
#include <unistd.h>
#include <sstream>
#include <algorithm>

using namespace cv;

class ColorParser {
private:
    std::string sub_topic;
    bool robot_order;
    bool offer_service;
    int robot_face_idx;
    ros::Subscriber img_sub;
    std::vector<geometry_msgs::Point> sq_positions;
    std::vector<double> sq_lengths;
    Mat cube_img;
    std::vector<Mat> cube_img_cs;
    std::vector<std::vector<std::vector<Vec3d> > > patches_cs;
    std::vector<std::vector<int> > assignments;
    std::vector<int> robot_face_order;
    std::vector<int> robot_rot_inc;
    void imgCallback(const cube_track::CubeTrack& msg);
    void extract(int face_id);
    bool solve_cube(void);
    int checkForExtract(bool wait=true);
    int checkCubeReady(void);
    int getch(void);
    double hsv_dist(Vec3d& candidate, Vec3d& center);
    double luv_dist(Vec3d& candidate, Vec3d& center);
    double dist(Vec3d& candidate, Vec3d& center);
    void get_neighbors(Point2i& query, std::vector<Point2i>& neighbors);
    void print_face(std::vector<int>& face);
    template<typename T>
    void rotateVector(std::vector<T>& elements, int n);
    void clearCubeState(void);
    std::string getSolutionString(void);
    bool extractCubeService(cube_track::ExtractCube::Request &req, cube_track::ExtractCube::Response& res);
    bool solveCubeService(cube_track::SolveCube::Request &req, cube_track::SolveCube::Response& res);
public:
    ColorParser(std::string& sub_topic, bool robot_order, bool offer_service, ros::NodeHandle& node);
    void start(ros::NodeHandle& node);
    ~ColorParser();
};

#endif // COLOR_PARSER_H
