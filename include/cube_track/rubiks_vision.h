#ifndef RUBIKS_VISION_H
#define RUBIKS_VISION_H

#define SCALE_FACTOR 1
#define R_FINGER_OFFSET 0.0
#define L_FINGER_OFFSET -0.0
#define CUBE_GRIP_WIDTH 0.09
#define HOUGH_LINE_QUOTA 15

#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <vector>
#include <string>
#include <sstream>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/image_encodings.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/video/tracking.hpp"
#include <opencv2/imgproc/imgproc.hpp>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Dense>
#include <actionlib/client/simple_action_client.h>
#include <pr2_controllers_msgs/PointHeadAction.h>
#include "cube_track/CubeTrack.h"

using namespace cv;

enum HoldingGripper {
    LEFT_GRIPPER,
    RIGHT_GRIPPER,
    NONE_GRIPPER
};

class RubiksVision {
private:
    std::string sub_topic;
    std::string pub_topic;
    std::string camera_info;
    std::string camera_link;
    ros::Subscriber img_sub;
    ros::Publisher cube_pub;
    Eigen::Matrix3d coord_to_pixel;
    tf::TransformListener tf_listener;
    int prev_detect_lines;
    int hough_thresh;
    actionlib::SimpleActionClient<pr2_controllers_msgs::PointHeadAction> point_head;
    void imgCallback(const sensor_msgs::Image& msg);
    void pointHead(double x, double y, double z, std::string& target_frame, std::string& ref_frame);
    HoldingGripper getHoldingGripper(void);
    Point2f getImgCoord(std::string& link);
    Point2f getImgCoord(std::string& link, double x_offset, double y_offset, double z_offset);
    void blackoutRegion(Mat& img, double slope, double offset1, double offset2, bool vertical=false);
    void blackoutCircle(Mat& img, Point2f& center, int radius);
    std::vector<Vec4i> findLines(Mat& img, int min_length);
    std::vector<Vec4i> findCenterSquare(std::vector<Vec4i>& lines);
    double ptDist(Point2f& p1, Point2f& p2);
    bool intersect(Vec4i& l1, Vec4i& l2);
    double findMatch(Mat& img, Mat& template_img, Point& match_loc);
    Point2f transformPoint(Point2f& p, double angle, Point2f& offset);
public:
    RubiksVision(std::string& sub_topic, std::string& pub_topic,  std::string& camera_info,
                 std::string& camera_link, ros::NodeHandle& node);
    ~RubiksVision();
};

#endif // RUBIKS_VISION_H
